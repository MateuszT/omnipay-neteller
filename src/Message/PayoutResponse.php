<?php

namespace Omnipay\Neteller\Message;

/**
 * Neteller Payout Response.
 *
 * @author    Alexander Fedra <contact@dercoder.at>
 * @copyright 2016 DerCoder
 * @license   http://opensource.org/licenses/mit-license.php MIT
 */
class PayoutResponse extends AbstractResponse
{

    public function getStatus()
    {
        if (!isset($this->data->transaction) || !isset($this->data->transaction->status)) {
            return null;
        }
        if($this->data->transaction->status === "accepted") {
            return 2;
        }
    }

    public function getTransactionId()
    {
        if (isset($this->data->transaction)) {
            return $this->data->transaction->merchantRefId;
        }
    }
}
