<?php

namespace Omnipay\Neteller\Message;

/**
 * Neteller Purchase Response.
 *
 * @author    Alexander Fedra <contact@dercoder.at>
 * @copyright 2016 DerCoder
 * @license   http://opensource.org/licenses/mit-license.php MIT
 */
class PurchaseResponse extends AbstractResponse
{
    public function getRedirectUrl()
    {
        if (!isset($this->data['links']) || !isset($this->data['links'][0])) {
            return null;
        }

        return (string) $this->data['links'][0]['url'];
    }
}
