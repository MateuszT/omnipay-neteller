<?php

namespace Omnipay\Neteller\Message;

/**
 * Neteller Purchase Response.
 *
 * @author    Alexander Fedra <contact@dercoder.at>
 * @copyright 2016 DerCoder
 * @license   http://opensource.org/licenses/mit-license.php MIT
 */
class PurResponse extends AbstractResponse
{
    public function getTransactionId()
    {
        if (!isset($this->data['merchantRefId'])) {
            return null;
        }

        return (string) $this->data['merchantRefId'];
    }

    public function getRedirectUrl()
    {
        foreach ($this->data['links'] as $link) {
            if ($link['rel'] === "hosted_payment") {
                return $link['url'];
            }
        }

        return null;
    }

}
